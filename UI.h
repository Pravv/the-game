#include <SFML/Graphics.hpp>

class UI
{
public:
	UI();
	~UI();

	sf::Sprite menuBackground;
	sf::Texture menuTxt;

	sf::Sprite background;
	sf::Texture bgTxt;
};