#include "Game.h"
#include "Player.h"
#include "vector"
#include "time.h"
#include <iostream>

std::vector<GameObject> Game::Enemies;
std::vector<GameObject> Game::GameObjectsBuffer;

void Game::Instantiate(GameObject& go)
{
	//Enemies.push_back(go);
	Enemies.push_back(go);
	std::cout<<"Added new GameObject [";
}
void Game::AddEnemy(GameObject& go)
{
	//Enemies.push_back(go);
	Enemies.push_back(go);
	std::cout<<"Added enemy: "<<go.name<<"\n";
}
void Game::Destroy(GameObject& go)
{
	for (std::vector<GameObject>::iterator it=Game::Enemies.begin(); it!=Game::Enemies.end(); )
	{
		if(it->gameObject==go.gameObject) 
	   {
		  it = Game::Enemies.erase(it);
		  std::cout<<"ERASE "<<go.name<<"!"<<" \n";
	   }
		else 
		  ++it;
	 }
}
void Game::RemoveAt(int index)
{
	Enemies[index] = Enemies.back();
	Enemies.pop_back();
	std::cout<<"Delete at: "<<index<<"\n";
	/*for(it = Enemies.begin(); it != Enemies.end(); it++)
	{
		std::cout << "vector element: " << *it << std::endl;
		if(*it==index)
			Enemies.erase(it);
	}*/
}

void Game::HitNearby(const Player& player)
{
	std::cout<<"Executing attack. \n";

	//TODO: Make a better loop for object erasing
	for(int i = 0; i < Enemies.size(); i++)
	{
		if(Enemies[i].gameObject == player.gameObject)
			continue;

		int damage = 0;

		
		damage = (std::rand() % player.maxDamage ) + player.minDamage;
		Enemies[i].ObtainAttack(damage);
	}
	/*
	for (auto& Enemy = Enemies.begin(); Enemy != Enemies.end(); ++Enemy)
    {  
		if (Enemy->gameObject == player.gameObject)
			continue;

		int damage = 0;

		damage = (std::rand() % player.maxDamage ) + player.minDamage;
		Enemy->ObtainAttack(50);
    }
	*/
}