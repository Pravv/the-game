#include "Time.h"
#include <iostream>

sf::Clock Time::deltaClock;
sf::Clock Time::clock;

float Time::deltaTime;
float Time::time;


void Time::UpdateTime()
{
	sf::Time time1 = clock.getElapsedTime();

	time = time1.asSeconds();

	sf::Time time2 = deltaClock.restart();

	deltaTime = time2.asSeconds();
}