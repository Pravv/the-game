#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <string>
#include <thread>
#include <chrono>
#include <iostream>
#include "Network.h"
#include <windows.h>

//using namespace std::chrono_literals;

int main()
{
	const unsigned short port = 50003;
	std::cout << "main thread!" << std::this_thread::get_id() << std::endl;
	Network server(port);
	

	sf::Uint16 x = 10;
	std::string s = "hello";
	double d = 0.6;

	sf::Packet packet;
	packet << x << s << d;

	int y = 0;
	Sleep(1000);
	Network client(port, "127.0.0.1");
	while (true)
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			// left key is pressed: move our character
			client.send(packet);
		}
		sf::Packet packet2;
		sf::Uint16 x;
		std::string s;
		double d;

		if (client.receive(packet2))
		{
			std::cout << "no data to receive\n";
			continue;
		}
		if (packet2 >> x >> s >> d) std::cout << "receive main: " << x << s << d << std::endl;
	}
}