#include "Enemy.h"
#include "Game.h"
#include <iostream>

Enemy::Enemy(float posX, float posY) : GameObject()
{
	name = "Enemy";
	gameObject->name = name;
	txt.loadFromFile("C:/Users/uczen/Desktop/enemy.jpg");
	sprite.setTexture(txt);
	sprite.setScale(0.2f, 0.2f);
	sprite.setOrigin(200, 200);


	curHealth = 100;

	sprite.setPosition(posX, posY);
	gameObject->position = sprite.getPosition();

	Game::Instantiate(*this->gameObject);

	std::cout<<" Name: "<< name << "] \n";
}