#include "Network.h"
#include <Windows.h>

Network::Network(const unsigned short & port)
{
	if (listener.listen(port) != sf::Socket::Done)
	{
		std::cout << "Shit went wrong! ABORT! ERROR: 453";
		delete this;
	}
	selector.add(listener);

	thread = new sf::Thread(&Network::listenForConnections, this);
	thread->launch();
}

Network::Network(const unsigned short &port, const sf::IpAddress &ip)
{

	sf::Socket::Status status = socketClient.connect(ip, port);
	if (status != sf::Socket::Done)
	{
    	// error...
	}

	std::cout << std::endl << "connected " << std::endl;
	socketClient.setBlocking(false); 
}

Network::~Network()
{
}

bool Network::receive(sf::Packet &packet)
{
	sf::Packet buffer;
	sf::TcpSocket::Status state = socketClient.receive(buffer);
	if (state == sf::Socket::Done)
	{
		sf::Uint16 x;
		std::string s;
		double d;
		packet = buffer;
		if (packet >> x >> s >> d) std::cout << "receive: " << x << s << d << std::endl;
		return true;
	}
	else if (state == sf::Socket::NotReady)
	{
		return false;
	}
}

void Network::send(sf::Packet packet)
{
	if (socketClient.send(packet) != sf::Socket::Done)
	{
		std::cout << "packet can not be send"<<std::endl;
	}
}

void Network::listenForConnections()
{
	std::cout << "started new thread!" << std::this_thread::get_id();
	while (true)
	{
		if (selector.wait())
		{
			if (selector.isReady(listener))
			{
				sf::TcpSocket* user = new sf::TcpSocket;
				if (listener.accept(*user) == sf::Socket::Done)
				{
					clients.push_back(user);
					selector.add(*user);
					std::cout << "User connected" << std::endl;
				}
				else
				{
					delete user;
				}
			}
			else
			{
				for (std::list<sf::TcpSocket*>::iterator it = clients.begin(); it != clients.end(); ++it)
				{
					sf::TcpSocket& client = **it;
					if (selector.isReady(client))
					{
						sf::Packet Data;
						sf::TcpSocket::Status State = client.receive(Data);
						if (State == sf::Socket::Done)
						{
							std::cout << "packet received" << std::endl;
							sendToAllSocketsExceptOne(client, data)
						}
						else if (State == sf::Socket::Disconnected)
						{
							std::cout << "disconnected" << std::endl;
							//PublicMessage(User.ClvientName + " has left the Chat.");
						}
						else if (State == sf::Socket::Error){
							std::cout << "Error" << std::endl;
						}
					}
				}
				clients.remove_if([](sf::TcpSocket* client){sf::Packet data; sf::TcpSocket::Status State = client->receive(data); return State == sf::Socket::Disconnected; });
			}
		}
	}
}
void Network::sendToAllSocketsExceptOne(sf::TcpSocket& dont, sf::Packet packet)
{
	for (std::list<sf::TcpSocket*>::iterator it = clients.begin(); it != clients.end(); ++it)
    {
    	sf::TcpSocket& client = **it;
		if (client.getRemoteAddress() != dont.getRemoteAddress())
		{
			if (client.send(packet) == sf::Socket::Done)
			{
				std::cout << "Packet sended";
			}
		}
	}
}
