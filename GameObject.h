#pragma once 
#include <SFML/Graphics.hpp>

class GameObject
{
public:
	GameObject();
	//~GameObject();

	std::string name;
	sf::Sprite sprite;
	sf::Vector2f position;

	sf::Font font;
	sf::Text text;

	int id;
	
	GameObject* gameObject;
	
	int curHealth;
	int maxHealth;
	//sf::Vector2f position;

	sf::Texture txt;
	void ObtainAttack(int damage);
	void Move(sf::Vector2f acceleration);
	void Destroy();
};