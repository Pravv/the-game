#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Enemy.h"
#include "UI.h"
#include "Time.h"
#include <vector>
#include "Game.h"
#include <iostream>
#include <sstream>

int main()
{
	int gameState = 0;
    // create the window
    sf::RenderWindow window(sf::VideoMode(800, 600), "Gra");

	srand(time(NULL));
	int randomX = std::rand() % 801;
	int randomY = std::rand() % 601;

	Player* p = new Player(randomX, randomY);

	UI* menuUi = new UI();

	Enemy* e = new Enemy(200, 200);
	Enemy* e1 = new Enemy(400, 200);
	Enemy* e2 = new Enemy(600, 200);
    // run the program as long as the window is open
    while (window.isOpen())
    {
        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
        while (window.pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
                window.close();
			if( event.type == event.MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left)
				p->Attack();
        }
        window.clear(sf::Color::Black);


		Time::UpdateTime();



		if(gameState == 0)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			{
				// left key is pressed: move our character
				gameState = 2;
			}
			window.draw(menuUi->menuBackground);
		}
		if(gameState == 2)
		{
			window.draw(p->sprite);
			p->Update();
			p->Rotate(window);

			//window.draw(p->sprite);

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::E))
			{
				// left key is pressed: move our character
				gameState = 0;
			}

			for(int i = 0; i < Game::Enemies.size(); i++)
			{
				if(Game::Enemies[i].gameObject == p->gameObject)
				{
					//Game::Enemies[i].
					continue;
				}
				window.draw(Game::Enemies[i].sprite);
				window.draw(Game::Enemies[i].text);
				Game::Enemies[i].text.setPosition(Game::Enemies[i].position);

				std::ostringstream HealthString;
				HealthString<<Game::Enemies[i].curHealth;

				std::string str = HealthString.str();

				Game::Enemies[i].text.setString(str);
			}
		}

        // end the current frame
        window.display();
    }

    return 0;
}

