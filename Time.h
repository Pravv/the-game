#include <SFML/Graphics.hpp>

class Time
{
public:
	Time();

	static sf::Clock deltaClock;
	static sf::Clock clock;

	static float time;
	static float deltaTime;

	static void UpdateTime();
private:
	
};