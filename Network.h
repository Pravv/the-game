#pragma once

#include <SFML/Network.hpp>
#include <string>
#include <thread>
#include <chrono>
#include <iostream>
#include <list>
class Network
{
public:
	Network(const unsigned short &port);
	Network(const unsigned short &port, const sf::IpAddress &ip);
	virtual ~Network();
	void send(sf::Packet packet);
	bool receive(sf::Packet &packet);

private:
	sf::TcpListener listener;
	sf::Thread *thread;
	sf::TcpSocket socketServer;
	sf::TcpSocket socketClient;
	std::list<sf::TcpSocket*> clients;
	sf::SocketSelector selector;
	void Network::listenForConnections();
	void sendToAllSocketsExceptOne(sf::TcpSocket& dont, sf::Packet packet);
};

