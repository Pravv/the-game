#pragma once 
#include "GameObject.h"


class Player : public GameObject
{
public:
	Player(float posX, float posY);
	~Player();

	float attackCooldown;

	int minDamage;
	int maxDamage;

	float criticalChance;

	//variables
	//int id;
	//std::string name;
	//GameObject* gameObject;

	//int classId;

	//voids
	void Update();
	void InitializeStats();
	void Movement();
	void Attack();
	void Rotate(sf::RenderWindow& window);
};