#include "Player.h"
#include "Game.h"
#include <iostream>
#include "Time.h"

Player::Player(float posX, float posY) : GameObject()
{
	name = "playah";
	gameObject->name = name;
	txt.loadFromFile("C:/Users/uczen/Desktop/player.jpg");
	sprite.setTexture(txt);
	sprite.setOrigin(16, 32);
	sprite.setPosition(posX, posY);

	//gameObject = new GameObject(name);
	//gameObject = this->gameObject;

	InitializeStats();

	Game::Instantiate(*this->gameObject);

	
	std::cout<<" Name: "<< gameObject->name << "] \n";
}
void Player::InitializeStats()
{
	attackCooldown = 0;

	minDamage = 5;
	maxDamage = 20;

	criticalChance = 30;
}
void Player::Update()
{
	Movement();

	if(attackCooldown > 0)
	{
		attackCooldown -= Time::deltaTime;
		//std::cout<<attackCooldown<<std::endl;
	}
}
void Player::Movement()
{
	float speed = 0.2f;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		// left key is pressed: move our character
		sprite.move(-speed, 0);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		// left key is pressed: move our character
		sprite.move(speed, 0);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		// left key is pressed: move our character
		sprite.move(0, -speed);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		// left key is pressed: move our character
		sprite.move(0, speed);
	}
}
void Player::Attack()
{
	if(attackCooldown <= 0)
	{
		Game::HitNearby(*this);
		attackCooldown = 1;
	}
}
void Player::Rotate(sf::RenderWindow& window)
{
	//std::cout<<sf::Mouse::getPosition(window).x << " | " << sprite.getPosition().x <<std::endl;
	if(sf::Mouse::getPosition(window).x > sprite.getPosition().x)
	{
		sprite.setScale(1,1);
	}
	else if(sf::Mouse::getPosition(window).x < sprite.getPosition().x)
	{
		sprite.setScale(-1,1);
	}
}