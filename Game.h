#include <SFML/Graphics.hpp>
#include "GameObject.h"
#include "Player.h"
#include <vector>

class Game
{
public:
	Game();

	static void HitNearby(const Player& player);
	static std::vector<GameObject> Enemies;
	static std::vector<GameObject> GameObjectsBuffer;
	static sf::Clock clock;

	static void RemoveAt(int index);
	static void AddEnemy(GameObject& go);
	static void Instantiate(GameObject& go);
	static void Destroy(GameObject& go);
};